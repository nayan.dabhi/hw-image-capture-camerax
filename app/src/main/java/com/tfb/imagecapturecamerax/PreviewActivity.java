package com.tfb.imagecapturecamerax;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

public class PreviewActivity extends AppCompatActivity {

    ImageView mImgPreview;
    public static final String IMG_URL = "img_url";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        mImgPreview = findViewById(R.id.mImgPreview);

        Bundle bundle = getIntent().getExtras();
        if(bundle == null){
            finish();
            return;
        }

        Uri uri = Uri.parse(bundle.getString(IMG_URL,""));

        if(uri!=null){
            mImgPreview.setImageURI(uri);
        }
    }
}